## Run a blog

Let's try a simple example with [Colt engine blog](https://gitlab.com/NalaGinrut/colt) which is a Git based blog system.
Of course, a blog is not a typical cloud native application, but it's simple enough to explain how does Alexon work.

```bash
alexon app create my-app.yaml
```

## Application manifest

And here's the **my-app.yaml**

```yaml
apiVersion: syme.dev/alexon/app/v1-alpha
kind: Application
name: my-blog
port: 8080
nodes:
  nodeFile:
    path: /etc/alexon/templates/colt.yaml
```

## Worker manifest

The **/etc/alexon/templates/colt.yaml** is a pre-defined worker node which is a simple blog engine.

```yaml
piVersion: syme.dev/alexon/worker/v1-alpha
kind: Worker
cluster: colt
nodeDef:
  name: colt
  image: registry.gitlab.com/nalaginrut/colt:latest
  workdir: /colt
  cmd: art work --refresh
  ports:
    - name: http
      bind: 3000:3000
```
*NOTE: If the 8080 port was reported to be occupied, please change a port.*

Please hold on for a moment, Alexon is finishing the work.

In this simple example, **even Alexon command was finished, you may still need a few seconds to let Colt blog engine bootup.**

Then open localhost:8080 with you browser.

Enjoy!
