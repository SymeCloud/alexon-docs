---
hide:
  - navigation
  - toc
---
<style>
img {
    margin-right: 100px;
    margin-bottom: 50px;
}
</style>

<center><img src="/img/alexon.png" alt="Alexon" width="330"></center>
<center>
<img src="/img/gplv3.png" alt="GPLv3" width="100">
<img src="/img/guile.png" alt="GNU Guile" width="200">
<img src="/img/kubernetes.svg" alt="Kubernetes" width="100">
<img src="/img/docker.png" alt="Docker" width="100">
</center>

## What is Alexon?

Alexon is an abstract layer above Docker and Kubernetes components, which hides the low-level details to make your quick deployment smoothly.
Alexon was written with GNU Guile, which is known for its support of functional programming concepts. As a result, Alexon is influenced by the functional programming paradigm

## Who needs Alexon?

If you're struggling against Kubernetes, or you're planning to make your product from a single WebApp to be a cloud native for the benefit of High-Availability and Scalability, you may take a look at Alexon. Alexon will handle Kubernetes, DNS, load balancing, and all the complications for you. You won't need to undergo extensive training to make your project cloud native. So that you can spend more time developing your product and improving the user experience. With Alexon, you can quickly and easily deploy your applications to a distributed infrastructure, without the need for extensive technical expertise of cloud native.

If you're the folks who care about decentralization and hope to deploy the cloud native product easily in your private cloud, you should consider Alexon.

## When to use Alexon?

Alexon is the easy tool for Web app/service in cloud native way. It's designed to quick deploy your project without spending much time on configuring Kubernetes componenets. Alexon provides customizable templates for your convenience.

Alexon is not for debugging, so you still need other popular tool like Kubectl to tweak and debug before the deployment.

## Where to use Alexon?

Both cloud and edge environment on GNU/Linux. Alexon supports K3s so that you should be able to use Alexon on devices like RaspberryPi.

## Why Alexon?

According to the investigation of CNCF ([read the report](https://www.cncf.io/reports/cncf-annual-survey-2022/)), the biggest challenges responders reported, in using and deploying containers, are **lack of training and security**. In fact, lack of training is the most significant barrier inhibiting adoption.

We believe the best training is no training at all. Think about it - the iPhone was praised so highly because no one needed training to use it. Why do we focus on training instead of finding ways to avoid the heavy training unless it's necessary?

## How to use Alexon?

It's in the classic cloud native way with 3 steps:

- Write manifest
- Copy to host machine
- Apply it with Alexon

For more details, please follow the [quick start](/quick_start).

## Story behind the name

In ancient Greece, Alexon was a brave mercenary from Achaea. In 250 BC, during the First Punic War, Alexon served in the Carthaginian army and guarded the fortress of Lilybaeum. At that time, some disloyal Gallic mercenaries planned to betray the fortress to the Romans. However, Alexon discovered the plot in time and reported it to the Carthaginian commander. He also persuaded other mercenaries to continue defending the fortress. Alexon's heroic act saved the fortress of Lilybaeum from falling into the hands of the Romans. (See [Wiki](https://en.wikipedia.org/wiki/Alexon))

The name Alexon implies **Protection** and **Loyalty**. Hope it serves you well.

## Design philosophy

Alexon plays the role of the leader of a "mercenary group" consists of Kubernetes, Docker, Nginx, CoreDNS and many other components.
Alexon provides a simplified solution for deploying and managing containerized applications. It is designed to simplify the process of building and deploying applications in a distributed environment, and is based on a microservices architecture that allows for easy scaling and high availability.
