## Quick install with Docker

This is the most easiest way to install Alexon.
Make sure you have installed Docker.

```bash
# assume you're in Debian/Ubuntu
sudo apt install docker-ce
```
Or you may follow [Docker official manual](https://docs.docker.com/engine/install/).

Then you may save code below to a file named **alexon**:

```bash
#!/bin/bash

docker run -it --rm \
       -v $PWD:/alexon \
       -v /var/run/docker.sock:/var/run/docker.sock \
       registry.gitlab.com/symecloud/alexon:latest \
       "$@"
```

Then make it executable:

```bash

sudo mv alexon /usr/local/bin/
sudo chmod +x /usr/local/bin/alexon

```

## Install from source code

First, you may download the source code:
```bash
git clone https://gitlab.com/SymeCloud/alexon.git
```
Or download the [release code](https://gitlab.com/SymeCloud/alexon/-/tags).

### Dependencies

Assuming that you are using Debian/Ubuntu:

```bash
sudo apt install docker-ce guile-3.0 guile-3.0-dev make autoconf automake libtool pkgconf bash util-linux curl
```
Then install necessary dependencies:

```bash
# In the current alexon source directory
sudo ./scripts/install-deps.sh --no-docker # since you've already installed docker
```

### Build

```bash
./configure
make
sudo make install
```
